import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { CartComponent } from './components/cart/cart.component';
import { ShippingComponent } from './components/shipping/shipping.component';
import { HomeComponent } from './components/home/home.component';
import { ItemDirectoryComponent } from './components/item-directory/item-directory.component';
import { LoginComponent } from './components/login/login.component';
import { NewItemComponent } from './components/new-item/new-item.component';

const routes: Routes = [
  { path: 'products', component: ProductListComponent },
  { path: 'products/:productId', component: ProductDetailsComponent },
  { path: 'cart', component: CartComponent },
  { path: 'shipping', component: ShippingComponent },
  { path:"new", component:NewItemComponent},
  { path:"directory", component:ItemDirectoryComponent},
  { path:"home", component:HomeComponent},
  { path:"login", component:LoginComponent},
  // { path:"", redirectTo: "/home", pathMatch: "full"}
  { path: '', redirectTo: 'products', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }