import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from "./app.component";
import { TopBarComponent } from "./components/top-bar/top-bar.component";
import { ProductListComponent } from "./components/product-list/product-list.component";
import { ProductAlertsComponent } from "./components/product-alerts/product-alerts.component";
import { ProductDetailsComponent } from "./components/product-details/product-details.component";
import { CartService } from './cart.service';
import { CartComponent } from './components/cart/cart.component';
import { ShippingComponent } from './components/shipping/shipping.component';
import { AuthService } from "./services/auth.service";
import { LoginComponent } from './components/login/login.component';
import { NewItemComponent } from './components/new-item/new-item.component';
import { HomeComponent } from './components/home/home.component';
import { ItemDirectoryComponent } from './components/item-directory/item-directory.component';
import { NavComponent } from './components/nav/nav.component';
import { AppRoutingModule } from "./app-routing.module";

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    TopBarComponent,
    ProductListComponent,
    ProductAlertsComponent,
    ProductDetailsComponent,
    CartComponent,
    ShippingComponent,
    LoginComponent,
    NewItemComponent,
    HomeComponent,
    ItemDirectoryComponent,
    NavComponent
  ],
  bootstrap: [AppComponent],
  providers: [
    CartService,
    AuthService  
  ]
})
export class AppModule {}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
