import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = [];

  addToCart(product) {
    this.items.push(product);
  }

  getItems() {
    return this.items;
  }

  clearCart() {
    this.items = [];
    return this.items;
  }

  getShippingPrices() {
    return this.http.get<{type: string, price: number}[]>('/assets/shipping.json');
  }

  getDistance(zipCode) {
    return this.http.get(
      `https://www.zipcodeapi.com/rest/yB24fYr6u0BDA6Ndu6sVTIwI0IsLf5Efj5YMBcULDFx2D5PLWgCaow6k7Wk2jpbz/distance/json/78754/${zipCode}/mile`
      );
  }

  constructor(
    private http: HttpClient
  ) { }

}