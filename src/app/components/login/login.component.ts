import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = "";
  password: string = "";
  message: string = "";

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    this.authService.attemptLogin(this.username, this.password)
      .subscribe((res) => {
        this.message = "Login successful";
        console.log(res.body.message);
        localStorage.setItem("token", res.headers.get("Authorization"));
        this.router.navigate(['products']);
      }, 
      (res) => {
        this.message = res.error.title;
    })
  }

}
