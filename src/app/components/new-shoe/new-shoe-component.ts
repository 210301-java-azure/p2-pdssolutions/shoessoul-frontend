import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Shoe } from 'src/app/models/shoe';
import { ShoeDataService } from 'src/app/services/shoe-data.service';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  shoe: Shoe = new Shoe();
  message: string = "";

  constructor(private shoeDataService: ShoeDataService) { }

  ngOnInit(): void {
  }

  createItem(){
    console.log(this.shoe);
    this.shoeDataService.addShoe(this.shoe).subscribe(
      ()=>{this.message = "Successfully added new shoe"},
      ()=>{this.message = "There was an issue adding your shoe"}
    )
  }

}
